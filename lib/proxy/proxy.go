package proxy

import (
	"fmt"
	"reflect"
)

func Invoke(proxy any, impl any) any {
	objType := reflect.TypeOf(proxy).Elem()
	objValue := reflect.ValueOf(proxy).Elem()
	numFIeld := objType.NumField()
	for i := 0; i < numFIeld; i++ {
		fileT := objType.Field(i)
		field := objValue.Field(i)
		method := reflect.MakeFunc(field.Type(), func(args []reflect.Value) []reflect.Value {
			fmt.Println("before ...")
			// 这里可以去调用其他的方法
			v := reflect.ValueOf(impl).MethodByName(fileT.Name)
			if !v.IsValid() {
				panic("can not found method " + fileT.Name)
			}

			result := v.Call(args)

			fmt.Println("after...")
			return result
		})
		field.Set(method)
	}
	return proxy
}
