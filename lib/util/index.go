package util

import (
	"bufio"
	"fmt"
	"os"
	"runtime"
	"strings"
)

func PassOut() {
	fmt.Println("\n\n按下 Enter 继续...")
	reader := bufio.NewReader(os.Stdin)
	_, _ = reader.ReadString('\n')
	fmt.Println("继续执行...")
}

// RunFuncName 获取当前执行的函数名称
func RunFuncName() string {
	pc := make([]uintptr, 1)

	runtime.Callers(2, pc)

	f := runtime.FuncForPC(pc[0])

	absPath := f.Name()

	split := strings.Split(absPath, "/")

	return split[len(split)-1]
}
