package object

import (
	"fmt"
	"reflect"
)

func ToString(arg any) {
	fmt.Println(InterfaceToString(arg))
}

func InterfaceToString(arg any) string {
	value := reflect.ValueOf(arg)
	result := ""

	if value.Kind() == reflect.Map {
		for _, key := range value.MapKeys() {
			field := key.Interface().(string)
			val := value.MapIndex(key).Interface()
			result += fmt.Sprintf("%s: %v, ", field, val)
		}
		return result
	}

	if IsBasicType(arg) == true {
		result = fmt.Sprintf("%v", value)
		return result
	}

	t := value.Type()
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		val := value.Field(i).Interface()
		result += fmt.Sprintf("%s: %v, ", field.Name, val)
	}
	return result
}

func IsBasicType(arg any) bool {
	value := reflect.ValueOf(arg)
	return value.Kind() >= reflect.Int && value.Kind() <= reflect.String
}
