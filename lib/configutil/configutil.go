package configutil

import (
	"gitee.com/zhc1226/zhclib/lib/pathutil"
)

// Deprecated:  从 zhclib 1.0.7 开始弃用
func GetAbsolutePath(path *string) string {
	return pathutil.GetAbsolutePath(path)
}

// Deprecated:  从 zhclib 1.0.7 开始弃用
// 获取程序执行的文件夹地址路径
func GetExecetableDirPath() string {
	return pathutil.GetExecetableDirPath()
}
