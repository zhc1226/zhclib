package fileutil

import (
	"bufio"
	"io"
	"os"

	"gitee.com/zhc1226/zhclib/lib/log"
)

// Exist 判断文件是否存在
func Exist(path string) bool {
	_, err := os.Stat(path)

	if err == nil {
		return true
	} else if os.IsNotExist(err) {
		return false
	} else {
		log.Error(log.Parse("校验文件{}是否存在时, 发生未知错误", path))
		return false
	}
}

// IsDir 判断是不是一个文件夹....
func IsDir(path string) bool {
	fi, err := os.Stat(path)

	if err != nil {
		if !Exist(path) {
			log.Info("获取文件信息出错, 文件不存在.")
		}
		return false
	}
	return fi.IsDir()
}

// IsFile 判断是不是文件
func IsFile(path string) bool {
	return !IsDir(path)
}

// ReadLine 文件按行读取
func ReadLine(file *os.File) ([]string, error) {
	reader := bufio.NewReader(file)
	result := make([]string, 0)
	for {
		line, _, err := reader.ReadLine()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}
		result = append(result, string(line))
	}
	return result, nil
}
