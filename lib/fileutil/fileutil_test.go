package fileutil

import (
	"fmt"
	"gitee.com/zhc1226/zhclib/lib/log"
	"testing"
)

type commStruct struct {
	Group         string // 分组
	input         string //输入
	ExpectedIndex string // 期望的属性
	Expected1     int64  // 期望值
	Expected2     string // 期望值
	Expected3     bool   // 期望值
}

var commTestData []commStruct

func TestExist(t *testing.T) {
	for _, v := range commTestData {
		if v.Group == "exist" {
			result := Exist(v.input)
			if result != v.Expected3 {
				t.Error(log.Parse("File {} Exist test failed", v.input))
			}
		}
	}
}

func TestIsFile(t *testing.T) {
	for _, data := range commTestData {
		if data.Group == "isFile" {
			result := IsFile(data.input)
			if result != data.Expected3 {
				t.Error(log.Parse("File {} IsFile test failed", data.input))
			}
		}
	}
}

func TestIsDir(t *testing.T) {
	for _, v := range commTestData {
		if v.Group == "isDir" {
			result := IsDir(v.input)
			if result != v.Expected3 {
				t.Error(log.Parse("Dir {} IsDir test failed", v.input))
			}
		}
	}
}

// 用于测试fileUitl.go
func TestMain(m *testing.M) {
	fmt.Println("test exec ... ")
	initCommonData()
	m.Run()
}

func initCommonData() {
	commTestData = []commStruct{
		{"exist", "test.json", "Expected3", 0, "1", false},
		{"exist", "fileutil.go", "Expected3", 0, "1", true},
		{"isFile", "fileutil.go", "", 0, "", true},
		{"isDir", "../fileutil", "", 0, "", true},
	}
}
