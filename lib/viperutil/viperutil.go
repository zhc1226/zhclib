package viperutil

import "github.com/spf13/viper"

const (
	// Config_type_YAML is the yml configuration file type
	Config_type_YAML = "yaml"
	Config_type_JSON = "json"
	Config_type_INI  = "ini"
)

func Init(configName string, configType string, configPath ...string) {

	viper.SetConfigName(configName)

	viper.SetConfigType(configType)

	for _, v := range configPath {
		viper.AddConfigPath(v)
	}

	if err := viper.ReadInConfig(); err != nil {
		panic(err)
	}

}
