package log

import (
	"fmt"
	"testing"
)

func TestParse(t *testing.T) {
	defer func() {
		a := recover()
		if a != nil {
			t.Error("test parse error")
		}
	}()
	Parse("test", 1213)
}

func TestMain(m *testing.M) {
	fmt.Println("test log....")
	m.Run()
}
