package log

import (
	"log"
	"os"
	"reflect"
	"strconv"
	"strings"
)

func Info(content any) {
	log.Printf("Info %v\n", content)
}

func Error(content any) {
	log.Printf("Error %v\n", content)
}

func Warn(content any) {
	log.Printf("Warn %v\n", content)
}

func Debug(content any) {
	log.Printf("Debug %v\n", content)
}

func Start() *os.File {
	logFile, _ := os.OpenFile("mylogs.log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666)
	log.SetOutput(logFile)
	return logFile
}

func Parse(input string, data ...any) string {
	if data == nil {
		Info(input)
		return input
	}
	for _, v := range data {
		input = strings.Replace(input, "{}", basicTypeToString(v), 1)
	}
	Info(input)
	return input
}

func basicTypeToString(data any) string {
	switch reflect.TypeOf(data).Kind() {
	case reflect.String:
		return data.(string)
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return strconv.Itoa(data.(int))
	case reflect.Bool:
		return strconv.FormatBool(data.(bool))
	case reflect.Float64:
		return strconv.FormatFloat(data.(float64), 'f', -1, 64)
	case reflect.Float32:
		return strconv.FormatFloat(float64(data.(float32)), 'f', -1, 32)
	default:
		return reflect.TypeOf(data).String()
	}
}
