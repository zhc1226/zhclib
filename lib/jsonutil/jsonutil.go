package jsonutil

import (
	"encoding/json"
	"gitee.com/zhc1226/zhclib/lib/log"
	"os"
	"reflect"
)

func ToJSONString(d any) string {
	result, err := json.Marshal(d)
	if err != nil {
		log.Error(err.Error())
		panic(err)
	}
	return string(result)
}

func Parse(json_str string, d any) {
	// 如果传入的d不是一个指针类型的数据, 那么这里会报错.
	if reflect.TypeOf(d).Kind() != reflect.Ptr {
		panic("d type cannot be ptr")
	}

	err := json.Unmarshal([]byte(json_str), d)
	if err != nil {
		log.Error(err.Error())
		panic(err)
	}
}

func ToJSONString_format(d any) string {
	result, err := json.MarshalIndent(d, "", " ")
	if err != nil {
		log.Error(err.Error())
		panic(err)
	}
	return string(result)
}

// Loads 加载一个json文件,转换成对应的d数据
func Loads(path string, d any) {
	content, err := os.ReadFile(path)
	if err != nil {
		log.Error(err.Error())
		panic(err)
	}
	Parse(string(content), d)
}
