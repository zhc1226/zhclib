package jsonutil

import (
	"fmt"
	"gitee.com/zhc1226/zhclib/lib/log"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
	"testing"
)

var commTestData []commStruct

type commStruct struct {
	Group    string // 分组
	input1   any    //输入
	input2   any    //输入
	Expected any    //期望值
}

type PersonTest struct {
	Name string `json:"name"`
}

func TestToJSONString(t *testing.T) {
	for _, v := range commTestData {
		if v.Group == "tojsonstring" {
			jsonString := ToJSONString(v.input1)
			jsonString = strings.ReplaceAll(jsonString, " ", "")
			expected := strings.ReplaceAll(v.Expected.(string), " ", "")
			if jsonString != expected {
				t.Error(log.Parse("ToJSONString expected got is error"))
			}
			log.Info(log.Parse("测试 ToJSONString 方法成功"))
		}
	}
}

func TestParse(t *testing.T) {
	for _, v := range commTestData {
		if v.Group == "parse" {
			Parse(v.input1.(string), v.input2)
			if v.input2.(*PersonTest).Name != v.Expected {
				t.Error("Parse expected got is error")
			}
			log.Info(log.Parse("测试 Parse 方法成功"))
		}
	}
}

func TestLoads(t *testing.T) {
	for _, v := range commTestData {
		if v.Group == "loads" {
			Loads(v.input1.(string), v.input2)
			if v.input2.(*PersonTest).Name != v.Expected {
				t.Error("Loads expected got is error")
			}
			log.Info(log.Parse("测试 loads 方法成功"))
		}
	}
}

func TestMain(m *testing.M) {
	fmt.Println("Testing main...")
	initComm()
	m.Run()

	removeTemp()
}

func initComm() {
	createJsonTemp()
	person := PersonTest{Name: "testname01"}
	person2 := new(PersonTest)
	commTestData = []commStruct{
		{Group: "tojsonstring", input1: person, input2: nil, Expected: "{\"name\": \"testname01\"}"},
		{Group: "parse", input1: "{\n  \"name\": \"test0002\"\n}", input2: &person, Expected: "test0002"},
		{Group: "loads", input1: "test.json.temp", input2: person2, Expected: "test"},
	}
}

func createJsonTemp() {
	// 创建一个临时文件, 写入数据
	file, _ := os.Create("test.json.temp")
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			panic(err)
		}
	}(file)
	_, err := fmt.Fprint(file, "{\"name\": \"test\"}")
	if err != nil {
		return
	}
	err = os.Remove("test.json")
	if err != nil {
		return
	}
}

func removeTemp() {
	// 遍历当前的目录
	filepath.Walk(".", func(path string, info fs.FileInfo, err error) error {
		if strings.HasSuffix(info.Name(), "temp") {
			os.Remove(path)
		}
		return nil
	})
}
