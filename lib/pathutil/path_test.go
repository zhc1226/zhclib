package pathutil

import (
	"gitee.com/zhc1226/zhclib/lib/log"
	"gitee.com/zhc1226/zhclib/lib/util"
	"path/filepath"
	"strings"
	"testing"
)

var CommTestData []CommStruct

type CommStruct struct {
	Group    string // 分组
	Input1   any    // 输入1
	Expected any    // 期待值

}

func TestGetAbsolutePath(t *testing.T) {
	for _, v := range CommTestData {
		if v.Group == "getAbsolutePath" {
			result := GetCurrentAbsolutePath(v.Input1.(string))
			if result == "" || !strings.HasSuffix(result, v.Input1.(string)) {
				t.Error("getAbsolutePath failed ...")
			}
			log.Parse("测试 {} 执行完成, 输入值 {} 期待结果 {}", util.RunFuncName(), v.Input1.(string), v.Expected)
		}
	}
}

func TestMain(m *testing.M) {
	initCommTestData()
	m.Run()

}

func initCommTestData() {
	absPath, _ := filepath.Abs(".")
	expected1 := filepath.FromSlash(filepath.Join(absPath, "path_test.go"))
	CommTestData = []CommStruct{
		{Group: "getAbsolutePath", Input1: "path_test.go", Expected: expected1},
	}
}
