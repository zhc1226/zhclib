package pathutil

import (
	"gitee.com/zhc1226/zhclib/lib/log"
	"os"
	"path/filepath"
)

// GetAbsolutePath
// Deprecated: pathutil.GetCurrentAbsolutePath
// 这个方法从1.0.7开始不推荐使用
// 新的方法推荐: see GetCurrentAbsolutePath
func GetAbsolutePath(path *string) string {
	if path == nil {
		return GetExecetableDirPath()
	}
	result := filepath.Join(GetExecetableDirPath(), *path)
	result = filepath.FromSlash(result)

	return result
}

func GetCurrentAbsolutePath(path string) string {
	result := filepath.Join(GetExecetableDirPath(), path)
	result = filepath.FromSlash(result)
	return result
}

// GetExecetableDirPath 获取程序执行的文件夹地址路径
func GetExecetableDirPath() string {
	execuPath, err := os.Executable()
	if err != nil {
		log.Error(err.Error())
		panic(err)
	}
	return filepath.FromSlash(filepath.Dir(execuPath))
}
