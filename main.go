package main

import (
	"fmt"
	"os"
)

func main() {
	// 判断一个文件是否存在
	_, err := os.Stat("test.json")

	if err == nil {
		fmt.Println("test.json is Exist")
	} else if os.IsNotExist(err) {
		f, _ := os.Create("test.json")
		defer f.Close()
		fmt.Println("test.json is not Exist")
	} else {
		fmt.Println("Error checking file: test.json")
	}

}
